﻿using System;

using TwilightSparkle.NetStandard.Interfaces;

namespace TwilightSparkle.NetStandard.Impementations
{
    public class FooService : IFooService
    {
        public void Foo()
        {
            Console.WriteLine("Standard FOO");
        }
    }
}
