﻿using System;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using TwilightSparkle.NetStandard.Impementations;
using TwilightSparkle.NetStandard.Interfaces;

namespace TwilightSparkle.NetCoreTests
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(builder =>
                builder.AddJsonFile("appsettings.json")
                    .AddEnvironmentVariables()
                    .AddCommandLine(args)
                    .Build())
                .ConfigureServices(services =>
                {
                    //setup DI
                    services.AddSingleton<IFooService, FooService>();
                })
                //Setup logging
                .ConfigureLogging((hostContext, builder) =>
                {
                    builder.ClearProviders();
                    builder.AddConfiguration(hostContext.Configuration.GetSection("Logging"));
                    builder.AddDebug();
                })
                .Build();

            var logger = host.Services.GetService<ILoggerFactory>()
                .CreateLogger<Program>();
            logger.LogDebug("Starting application");

            Console.WriteLine("Start");

            var bar = host.Services.GetRequiredService<IFooService>();
            bar.Foo();

            Console.WriteLine("End");

            host.Run();
        }
    }
}
