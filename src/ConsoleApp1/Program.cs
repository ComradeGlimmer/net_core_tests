﻿using System;
using TwilightSparkle.NetStandard.Impementations;

namespace TwilightSparkle.Framework
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");

            var foo = new FooService();

            Console.WriteLine("End");
            Console.ReadKey();
        }
    }
}
